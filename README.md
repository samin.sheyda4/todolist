# TodoList

This is a code challenge for Has.to.be company! 

## State management
 for state management I used Bloc Pattern.
 Business logic components (BLoC) allow you to separate the business logic from the UI.
  Writing code in BLoC makes it easier to write and reuse tests.

## Local data base
 For saving data in local storage I create SQL data base suing Moor.
 Moor is a reactive persistence library for Flutter and Dart, built ontop of sqlite.

## Run the pipeline
To build the app and run the pipeline you need to push an empty commit to this repository.

**apk file and ios installation file will perpare by CodeMagic CI/CD in every pipeline of GitLab CI/CD**
