import 'package:moor/ffi.dart';
import 'package:test/test.dart';
import 'package:todolist/data/tasks_database.dart';
import 'package:todolist/models/task.dart';

void main() {
  late AppDatabase database;
  group("local Database tests", ()
  {
    setUpAll(() async {
      database = AppDatabase(VmDatabase.memory());
      await database.insertTask(Task("Task1", note: "details 1").toEntity());
    });
    tearDownAll(() async {
      await database.close();
    });

    test('read correct data from database', () async {
      List<TaskItem> tasks = await database.getAllTasks();
      expect(tasks.length, 1);
    });
    test('Update data should work fine database', () async {
      List<TaskItem> tasks = await database.getAllTasks();
      expect(tasks[0].title, "Task1");
      await database.updateTask(tasks[0].copyWith(title: "Task Updated 1"));
      tasks = await database.getAllTasks();
      expect(tasks[0].title, "Task Updated 1");
    });
    test('delete data should work fine database', () async {
      List<TaskItem> tasks = await database.getAllTasks();
      await database.deleteTask(tasks[0]);
      tasks = await database.getAllTasks();
      expect(tasks.length, 0);
    });
  });
}

