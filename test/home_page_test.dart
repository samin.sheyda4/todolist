import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:todolist/bloc/filtered_tasks/filtered_tasks_bloc.dart';
import 'package:todolist/bloc/tab/tab_bloc.dart';
import 'package:todolist/bloc/tab/tab_event.dart';
import 'package:todolist/models/app_tab.dart';
import 'package:todolist/models/task.dart';
import 'package:todolist/models/visibility_filter.dart';
import 'package:todolist/presentation/pages/home_page.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';
import 'package:todolist/presentation/widgets/filtered_tasks.dart';

class TabBlocMock extends MockBloc<TabEvent, AppTab> implements TabBloc {}

class FilteredTasksBlocMock
    extends MockBloc<FilteredTasksEvent, FilteredTasksState>
    implements FilteredTasksBloc {} // extend MockBloc rather than Mock

class TabEventFake extends Fake implements TabEvent {}

class FilteredTasksEventFake extends Fake implements FilteredTasksEvent {}

class FilteredTasksStateFake extends Fake implements FilteredTasksState {}

class TaskBlocMock extends MockBloc<TasksEvent, TasksState>
    implements TasksBloc {} // extend MockBloc rather than Mock

class TasksStateFake extends Fake implements TasksState {}

class TasksEventFake extends Fake implements TasksEvent {}

void main() {
  setUpAll(() {
    registerFallbackValue<TabEvent>(TabEventFake());
    registerFallbackValue<FilteredTasksEvent>(FilteredTasksEventFake());
    registerFallbackValue<FilteredTasksState>(FilteredTasksStateFake());
    registerFallbackValue<TasksState>(TasksStateFake());
    registerFallbackValue<TasksEvent>(TasksEventFake());
    registerFallbackValue(AppTab.todos);
  });

  testWidgets('should show correct data in details page',
      (WidgetTester tester) async {
    final tabsBloc = TabBlocMock();
    when(() => tabsBloc.state).thenReturn(AppTab.todos);

    final tasksBloc = TaskBlocMock();
    when(() => tasksBloc.state).thenReturn(TasksLoadSuccess([
      Task("Task1", note: "Task details 1", id: "qwertyuiop"),
      Task("Task2", note: "Task details 2", id: "asddfghj")
    ]));

    //show all tasks on screen
    final filterTaskBloc = FilteredTasksBlocMock();
    when(() => filterTaskBloc.state).thenReturn(FilteredTasksLoadSuccess([
      Task("Task1", note: "Task details 1", id: "qwertyuiop"),
      Task("Task2", note: "Task details 2", id: "asddfghj")
    ], VisibilityFilter.all));

    await tester.pumpWidget(
      BlocProvider<TabBloc>(
        create: (c) => tabsBloc,
        child: BlocProvider<FilteredTasksBloc>(
          create: (c) => filterTaskBloc,
          child: BlocProvider<TasksBloc>(
            create: (c) => tasksBloc,
            child: MaterialApp(
              home: Scaffold(
                body: HomePage(),
              ),
            ),
          ),
        ),
      ),
    );
    await tester.idle();
    await tester.pump();

    // expect to have one add icon in screen
    expect(
        find.widgetWithIcon(FloatingActionButton, Icons.add), findsOneWidget);

    // expect to do not have any statistics widget in screen, since the selected tab is todos
    expect(find.byType(State), findsNothing);

    // expect to have FilteredTasks widget in screen since the selected tab is todos
    expect(find.byType(FilteredTasks), findsOneWidget);
  });
}
