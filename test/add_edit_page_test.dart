import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todolist/presentation/pages/add_edit_page.dart';

void main() {
  group("edit/add page tests", () {

    testWidgets(
        "should be one widget with add icon in the screen if user is adding new task",
        (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: AddEditPage(
          isEditing: false,
          onSave: (String task, String note) {},
        ),
      ));
      await tester.idle();
      await tester.pump();

      expect(
          find.widgetWithIcon(FloatingActionButton, Icons.add), findsOneWidget);
    });

    testWidgets(
        "should be one widget with edit icon in the screen if user is editing existing task",
        (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: AddEditPage(
          isEditing: true,
          onSave: (String task, String note) {},
        ),
      ));
      await tester.idle();
      await tester.pump();

      expect(
          find.widgetWithIcon(FloatingActionButton, Icons.check), findsOneWidget);
    });
  });
}
