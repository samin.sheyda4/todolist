import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';
import 'package:todolist/models/task.dart';
import 'package:todolist/presentation/pages/details_page.dart';

class TaskBlocMock extends MockBloc<TasksEvent, TasksState>
    implements TasksBloc {} // extend MockBloc rather than Mock

class TasksStateFake extends Fake implements TasksState {}

class TasksEventFake extends Fake implements TasksEvent {}

void main() {
  setUpAll(() {
    registerFallbackValue<TasksState>(TasksStateFake());
    registerFallbackValue<TasksEvent>(TasksEventFake());
  });

  testWidgets('should show correct data in details page', (WidgetTester tester) async {
    final tasksBloc = TaskBlocMock();
    when(() => tasksBloc.state).thenReturn(TasksLoadSuccess([
      Task("Task1", note: "Task details 1", id: "qwertyuiop"),
      Task("Task2", note: "Task details 2", id: "asddfghj")
    ])); // stub state rather than initialState

    await tester.pumpWidget(
      BlocProvider<TasksBloc>(
          create: (c) => tasksBloc,
          child: MaterialApp(
            home: DetailsPage(
              id: "qwertyuiop",
            ),
          )),
    );
    await tester.pump(Duration.zero);
    expect(find.text('Task details 1'), findsOneWidget);
    expect(find.text('Task1'), findsOneWidget);
  });
}
