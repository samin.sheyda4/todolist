import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist/presentation/pages/add_edit_page.dart';
import 'package:todolist/presentation/pages/home_page.dart';
import 'package:todolist/presentation/pages/splash_page.dart';

import 'bloc/filtered_tasks/filtered_tasks_bloc.dart';
import 'bloc/simple_bloc_observer.dart';
import 'bloc/stats/stats_bloc.dart';
import 'bloc/tab/tab_bloc.dart';
import 'bloc/tasks/tasks_bloc.dart';
import 'models/task.dart';

void main() {
  // We can set a Bloc's observer to an instance of `SimpleBlocObserver`.
  // This will allow us to handle all transitions and errors in SimpleBlocObserver.
  Bloc.observer = SimpleBlocObserver();
  runApp(
    BlocProvider(
      create: (context) {
        return TasksBloc()..add(TasksLoaded());
      },
      child: TodosApp(),
    ),
  );
}

class TodosApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "",
      initialRoute: SplashPage.routeName,
      routes: <String, WidgetBuilder>{
        HomePage.routeName: (BuildContext context) => MultiBlocProvider(
              providers: [
                BlocProvider<TabBloc>(
                  create: (context) => TabBloc(),
                ),
                BlocProvider<FilteredTasksBloc>(
                  create: (context) => FilteredTasksBloc(
                    tasksBloc: BlocProvider.of<TasksBloc>(context),
                  ),
                ),
                BlocProvider<StatsBloc>(
                  create: (context) => StatsBloc(
                    tasksBloc: BlocProvider.of<TasksBloc>(context),
                  ),
                ),
              ],
              child: HomePage(),
            ),
        SplashPage.routeName: (BuildContext context) => SplashPage(),
        AddEditPage.routeName: (BuildContext context) => AddEditPage(
              onSave: (task, note) {
                BlocProvider.of<TasksBloc>(context).add(
                  TaskAdded(Task(task, note: note)),
                );
              },
              isEditing: false,
            ),
      },
    );
  }
}
