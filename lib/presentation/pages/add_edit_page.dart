import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todolist/models/task.dart';


typedef OnSaveCallback = Function(String task, String note);

class AddEditPage extends StatefulWidget {
  static const String routeName = 'addEdit';
  final bool isEditing;
  final OnSaveCallback onSave;
  final Task? task;

  AddEditPage({
    Key? key,
    required this.onSave,
    required this.isEditing,
    this.task,
  }) : super(key: key);

  @override
  _AddEditPageState createState() => _AddEditPageState();
}

class _AddEditPageState extends State<AddEditPage> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

   String? _task;
   String? _note;

  bool get isEditing => widget.isEditing;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          isEditing ? "Edit task" : "Add task",
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(32.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              TextFormField(
                initialValue: isEditing ? widget.task?.title : '',
                autofocus: !isEditing,
                style: textTheme.headline5,
                decoration: InputDecoration(
                  hintText: "What needs to be done?",
                ),
                validator: (val) {
                  return val!.trim().isEmpty
                      ? "This task has no description"
                      : null;
                },
                onSaved: (value) => _task = value,
              ),
              SizedBox(height: 32,),
              TextFormField(
                initialValue: isEditing ? widget.task?.note : '',
                maxLines: 3,
                style: textTheme.subtitle1,
                decoration: InputDecoration(
                  hintText:"Please add some description",
                ),
                onSaved: (value) => _note = value,
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key:
            isEditing ? Key("saveTodoFab") : Key("saveNewTodo"),
        tooltip: isEditing ?" Save changes" : "Add task",
        child: Icon(isEditing ? Icons.check : Icons.add),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onSave(_task!, _note!);
            Navigator.pop(context);
          }
        },
      ),
    );
  }
}
