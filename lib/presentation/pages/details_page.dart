import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';

import 'package:todolist/models/task.dart';

import 'add_edit_page.dart';


class DetailsPage extends StatelessWidget {
  final String id;

  DetailsPage({Key? key, required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        final todo = (state as TasksLoadSuccess)
            .tasks
            .firstWhere((todo) => todo.id == id, orElse: () =>Task("title"));
        return Scaffold(
          appBar: AppBar(
            title: Text("Task details"),
            actions: [
              IconButton(
                tooltip: "Delete task",
                icon: Icon(Icons.delete),
                onPressed: () {
                  BlocProvider.of<TasksBloc>(context).add(TaskDeleted(todo));
                  Navigator.pop(context, todo);
                },
              )
            ],
          ),
          body: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: ListView(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 8.0),
                            child: Checkbox(
                                value: todo.complete,
                                onChanged: (_) {
                                  BlocProvider.of<TasksBloc>(context).add(
                                    TaskUpdated(
                                      todo.copyWith(complete: !todo.complete),
                                    ),
                                  );
                                }),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Hero(
                                  tag: '${todo.id}__heroTag',
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                      top: 8.0,
                                      bottom: 16.0,
                                    ),
                                    child: Text(
                                      todo.title,
                                      style:
                                          Theme.of(context).textTheme.headline5,
                                    ),
                                  ),
                                ),
                                Text(
                                  todo.note,
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
          floatingActionButton: FloatingActionButton(

            tooltip: "Edit task",
            child: Icon(Icons.edit),
            onPressed:  () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return AddEditPage(
                            onSave: (task, note) {
                              BlocProvider.of<TasksBloc>(context).add(
                                TaskUpdated(
                                  todo.copyWith(title: task, note: note),
                                ),
                              );
                            },
                            isEditing: true,
                            task: todo,
                          );
                        },
                      ),
                    );
                  },
          ),
        );
      },
    );
  }
}
