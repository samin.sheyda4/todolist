import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todolist/presentation/pages/home_page.dart';

class SplashPage extends StatelessWidget {
  static const String routeName = 'splash';

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 5000), () {
      Navigator.pushReplacementNamed(context, HomePage.routeName);
    });
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.blue,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'This is a Test app for'),
                        TextSpan(
                            text: ' Has.To.Be ',
                            style: new TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: 'code challenge.'),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Powered by. Samin Sheyda",
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.orange,
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(
            child: Container(
              child: Image.asset('assets/logo.png'),
            ),
          ),
        ],
      )),
    );
  }
}
