import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist/bloc/tab/tab_bloc.dart';
import 'package:todolist/bloc/tab/tab_event.dart';
import 'package:todolist/models/app_tab.dart';
import 'package:todolist/presentation/pages/add_edit_page.dart';
import 'package:todolist/presentation/widgets/extra_actions.dart';
import 'package:todolist/presentation/widgets/filter_button.dart';
import 'package:todolist/presentation/widgets/filtered_tasks.dart';
import 'package:todolist/presentation/widgets/stats.dart';
import 'package:todolist/presentation/widgets/tab_selector.dart';

class HomePage extends StatelessWidget {
  static const String routeName = 'home';
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(
      builder: (context, activeTab) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Has.To.Be Todo List"),
            actions: [
              FilterButton(visible: activeTab == AppTab.todos),
              ExtraActions(),
            ],
          ),
          body: activeTab == AppTab.todos ? FilteredTasks() : Stats(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, AddEditPage.routeName);
            },
            child: Icon(Icons.add),
            tooltip: "Add task",
          ),
          bottomNavigationBar: TabSelector(
            activeTab: activeTab,
            onTabSelected: (tab) =>
                BlocProvider.of<TabBloc>(context).add(TabUpdated(tab)),
          ),
        );
      },
    );
  }
}