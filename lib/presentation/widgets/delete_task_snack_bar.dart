import 'package:flutter/material.dart';
import 'package:todolist/models/task.dart';


class DeleteTaskSnackBar extends SnackBar {
  DeleteTaskSnackBar({
    Key? key,
    required Task task,
    required VoidCallback onUndo,
  }) : super(
          key: key,
          content: Text(
          "The task ${task.title} deleted ",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label:"Undo",
            onPressed: onUndo,
          ),
        );
}
