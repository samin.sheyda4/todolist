import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist/bloc/filtered_tasks/filtered_tasks_bloc.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';
import 'package:todolist/presentation/pages/details_page.dart';
import 'package:todolist/presentation/widgets/delete_task_snack_bar.dart';
import 'package:todolist/presentation/widgets/loading_indicator.dart';
import 'package:todolist/presentation/widgets/task_item.dart';


class FilteredTasks extends StatelessWidget {
  FilteredTasks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<FilteredTasksBloc, FilteredTasksState>(
      builder: (context, state) {
        if (state is FilteredTasksLoadInProgress) {
          return LoadingIndicator();
        } else if (state is FilteredTasksLoadSuccess) {
          final tasks = state.filteredTasks;
          return ListView.builder(
            itemCount: tasks.length,
            itemBuilder: (BuildContext context, int index) {
              final task = tasks[index];
              return TaskItem(
                task: task,
                onDismissed: (direction) {
                  BlocProvider.of<TasksBloc>(context).add(TaskDeleted(task));
                  ScaffoldMessenger.of(context).showSnackBar(
                    DeleteTaskSnackBar(
                      task: task,
                      onUndo: () => BlocProvider.of<TasksBloc>(context)
                          .add(TaskAdded(task)),
                    ),
                  );
                },
                onTap: () async {
                  final removedTodo = await Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) {
                      return DetailsPage(id: task.id);
                    }),
                  );
                  if (removedTodo != null) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      DeleteTaskSnackBar(
                        task: task,
                        onUndo: () => BlocProvider.of<TasksBloc>(context)
                            .add(TaskAdded(task)),
                      ),
                    );
                  }
                },
                onCheckboxChanged: (_) {
                  BlocProvider.of<TasksBloc>(context).add(
                    TaskUpdated(task.copyWith(complete: !task.complete)),
                  );
                },
              );
            },
          );
        } else {
          return Container();
        }
      },
    );
  }
}
