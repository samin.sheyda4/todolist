import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todolist/models/task.dart';


class TaskItem extends StatelessWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
   final ValueChanged<bool?>? onCheckboxChanged;
  final Task task;

  TaskItem({
    Key? key,
    required this.onDismissed,
    required this.onTap,
    required this.onCheckboxChanged,
    required this.task,
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(task.id.toString()),
      onDismissed: onDismissed,
      child: ListTile(
        onTap: onTap,
        leading: Checkbox(
          key: Key(task.id.toString()),
          value: task.complete,
          onChanged: onCheckboxChanged,
        ),
        title: Hero(
          tag: '${task.id}__heroTag',
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Text(
              task.title,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
        ),
        subtitle: task.note.isNotEmpty
            ? Text(
                task.note,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.subtitle1,
              )
            : null,
      ),
    );
  }
}
