import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';
import 'package:todolist/models/extra_action.dart';


class ExtraActions extends StatelessWidget {
  ExtraActions({Key? key}) : super();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        if (state is TasksLoadSuccess) {
          bool allComplete =
              (BlocProvider.of<TasksBloc>(context).state as TasksLoadSuccess)
                  .tasks
                  .every((todo) => todo.complete);
          return PopupMenuButton<ExtraAction>(
            onSelected: (action) {
              switch (action) {
                case ExtraAction.clearCompleted:
                  BlocProvider.of<TasksBloc>(context).add(ClearCompleted());
                  break;
                case ExtraAction.toggleAllComplete:
                  BlocProvider.of<TasksBloc>(context).add(ToggleAll());
                  break;
              }
            },
            itemBuilder: (BuildContext context) => <PopupMenuItem<ExtraAction>>[
              PopupMenuItem<ExtraAction>(
                value: ExtraAction.toggleAllComplete,
                child: Text(
                  allComplete
                      ? "Mark all Incomplete"
                      : "Mark all Complete",
                ),
              ),
              PopupMenuItem<ExtraAction>(
                value: ExtraAction.clearCompleted,
                child: Text(
                  "Clear completed tasks",
                ),
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}
