import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:uuid/uuid.dart';

// Moor works by source gen. This file will all the generated code.
part 'tasks_database.g.dart';

// The name of the database table is "TaskItems"
// By default, the name of the generated data class will be "TaskItem" (without "s")
class TaskItems extends Table {
  // autoIncrement automatically sets this to be the primary key
  TextColumn get id => text().clientDefault(() =>Uuid().v4())();
  // If the length constraint is not fulfilled, the Task will not
  // be inserted into the database and an exception will be thrown.
  TextColumn get title => text().withLength(min: 1, max: 50)();
  // DateTime is not natively supported by SQLite
  // Moor converts it to & from UNIX seconds
  TextColumn get details => text().withLength(min: 1, max: 200)();
  // Booleans are not supported as well, Moor converts them to integers
  // Simple default values are specified as Constants
  BoolColumn get completed => boolean().withDefault(Constant(false))();


  @override
  Set<Column> get primaryKey => {id};
}
// This annotation tells the code generator which tables this DB works with
@UseMoor(tables: [TaskItems])
// _$AppDatabase is the name of the generated class
class AppDatabase extends _$AppDatabase {
  AppDatabase(QueryExecutor e) : super(e);
  // AppDatabase()
  // // Specify the location of the database file
  //     : super((FlutterQueryExecutor.inDatabaseFolder(
  //   path: 'db.sqlite',
  //   // Good for debugging - prints SQL in the console
  //   logStatements: true,
  // )));

  // Bump this when changing tables and columns.
  // Migrations will be covered in the next part.
  int get schemaVersion => 1;


  Future insertTask(TaskItem task) => into(taskItems).insert(task);

  // Updates a Task with a matching primary key
  Future updateTask(TaskItem task) => update(taskItems).replace(task);

  Future deleteTask(TaskItem task) => delete(taskItems).delete(task);

  // All tables have getters in the generated class - we can select the tasks table
  Future<List<TaskItem>> getAllTasks() => select(taskItems).get();

  // Moor supports Streams which emit elements when the watched data changes
  Stream<List<TaskItem>> watchAllTasks() => select(taskItems).watch();
}