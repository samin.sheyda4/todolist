import 'dart:async';
import 'dart:core';

import 'package:moor_flutter/moor_flutter.dart';
import 'package:todolist/data/tasks_database.dart';

/// A class that Loads and Persists tasks. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as tasks_repository_local or tasks_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment
abstract class TasksRepository {
  /// Loads tasks first from Local database
  Future<List<TaskItem>> loadTasks();

  /// Persists tasks to local database
  Future saveTask(TaskItem task);

  Future updateTask(TaskItem task);

  Future deleteTask(TaskItem task);
}

class TasksLocalRepository implements TasksRepository {
  AppDatabase appDatabase = AppDatabase((FlutterQueryExecutor.inDatabaseFolder(
    path: 'db.sqlite',
    // Good for debugging - prints SQL in the console
    logStatements: true,
  )));

  @override
  Future<List<TaskItem>> loadTasks() async {
    try {
      return await appDatabase.getAllTasks();
    } catch (e) {
      return [];
    }
  }

  ///Persists tasks to local database
  @override
  Future saveTask(TaskItem taskItem) => appDatabase.insertTask(taskItem);

  @override
  Future updateTask(TaskItem taskItem) => appDatabase.updateTask(taskItem);

  @override
  Future deleteTask(TaskItem taskItem) => appDatabase.deleteTask(taskItem);
}
