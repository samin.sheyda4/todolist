part of 'tasks_bloc.dart';

abstract class TasksEvent extends Equatable {
  const TasksEvent();

  @override
  List<Object> get props => [];
}

class TasksLoaded extends TasksEvent {}

class TaskAdded extends TasksEvent {
  final Task task;

  const TaskAdded(this.task);

  @override
  List<Object> get props => [task];

  @override
  String toString() => 'TaskAdded { task: $task }';
}

class TaskUpdated extends TasksEvent {
  final Task task;

  const TaskUpdated(this.task);

  @override
  List<Object> get props => [task];

  @override
  String toString() => 'TaskUpdated { updatedTask: $task }';
}

class TaskDeleted extends TasksEvent {
  final Task task;

  const TaskDeleted(this.task);

  @override
  List<Object> get props => [task];

  @override
  String toString() => 'TaskDeleted { task: $task }';
}

class ClearCompleted extends TasksEvent {}

class ToggleAll extends TasksEvent {}
