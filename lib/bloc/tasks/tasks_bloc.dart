import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todolist/data/tasks_repository.dart';
import 'package:todolist/models/task.dart';
part 'tasks_event.dart';
part 'tasks_state.dart';

class TasksBloc extends Bloc<TasksEvent, TasksState> {
  final TasksLocalRepository todosRepository = TasksLocalRepository();

  TasksBloc() : super(TasksLoadInProgress());

  @override
  Stream<TasksState> mapEventToState(TasksEvent event) async* {
    if (event is TasksLoaded) {
      yield* _mapTasksLoadedToState();
    } else if (event is TaskAdded) {
      yield* _mapTaskAddedToState(event);
    } else if (event is TaskUpdated) {
      yield* _mapTaskUpdatedToState(event);
    } else if (event is TaskDeleted) {
      yield* _mapTaskDeletedToState(event);
    } else if (event is ToggleAll) {
      yield* _mapToggleAllToState();
    } else if (event is ClearCompleted) {
      yield* _mapClearCompletedToState();
    }
  }

  Stream<TasksState> _mapTasksLoadedToState() async* {
    try {
      final tasks = await this.todosRepository.loadTasks();
      yield TasksLoadSuccess(
        tasks.map(Task.fromEntity).toList(),
      );
    } catch (_) {
      yield TasksLoadFailure();
    }
  }

  Stream<TasksState> _mapTaskAddedToState(TaskAdded event) async* {
    if (state is TasksLoadSuccess) {
      final List<Task> updatedTasks =
          List.from((state as TasksLoadSuccess).tasks)..add(event.task);
      yield TasksLoadSuccess(updatedTasks);
      todosRepository.saveTask(event.task.toEntity());
    }
  }

  Stream<TasksState> _mapTaskUpdatedToState(TaskUpdated event) async* {
    if (state is TasksLoadSuccess) {
      final List<Task> updatedTasks =
          (state as TasksLoadSuccess).tasks.map((task) {
        return task.id == event.task.id ? event.task : task;
      }).toList();
      yield TasksLoadSuccess(updatedTasks);
      todosRepository.updateTask(event.task.toEntity());
    }
  }

  Stream<TasksState> _mapTaskDeletedToState(TaskDeleted event) async* {
    if (state is TasksLoadSuccess) {
      final updatedTasks = (state as TasksLoadSuccess)
          .tasks
          .where((task) => task.id != event.task.id)
          .toList();
      yield TasksLoadSuccess(updatedTasks);
      todosRepository.deleteTask(event.task.toEntity());
    }
  }

  Stream<TasksState> _mapToggleAllToState() async* {
    if (state is TasksLoadSuccess) {
      final allComplete =
          (state as TasksLoadSuccess).tasks.every((task) => task.complete);
      final List<Task> updatedTasks =
          (state as TasksLoadSuccess).tasks.map((task) {
        Task complete = task.copyWith(complete: !allComplete);
        todosRepository.updateTask(complete.toEntity());
        return complete;
      }).toList();
      yield TasksLoadSuccess(updatedTasks);
    }
  }

  Stream<TasksState> _mapClearCompletedToState() async* {
    if (state is TasksLoadSuccess) {
      final List<Task> updatedTasks = [];
      (state as TasksLoadSuccess).tasks.forEach((element) {
        !element.complete
            ? updatedTasks.add(element)
            : todosRepository.deleteTask(element.toEntity());
      });
      yield TasksLoadSuccess(updatedTasks);
    }
  }
}
