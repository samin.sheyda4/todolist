import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:todolist/bloc/tasks/tasks_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todolist/models/task.dart';

part 'stats_event.dart';
part 'stats_state.dart';



class StatsBloc extends Bloc<StatsEvent, StatsState> {
  final TasksBloc tasksBloc;
  late StreamSubscription tasksSubscription;

  StatsBloc({required this.tasksBloc}) : super(StatsLoadInProgress()) {
    void onTasksStateChanged(state) {
      if (state is TasksLoadSuccess) {
        add(StatsUpdated(state.tasks));
      }
    }

    onTasksStateChanged(tasksBloc.state);
    tasksSubscription = tasksBloc.stream.listen(onTasksStateChanged);
  }

  @override
  Stream<StatsState> mapEventToState(StatsEvent event) async* {
    if (event is StatsUpdated) {
      final numActive =
          event.tasks.where((task) => !task.complete).toList().length;
      final numCompleted =
          event.tasks.where((task) => task.complete).toList().length;
      yield StatsLoadSuccess(numActive, numCompleted);
    }
  }

  @override
  Future<void> close() {
    tasksSubscription.cancel();
    return super.close();
  }
}
