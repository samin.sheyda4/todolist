import 'package:equatable/equatable.dart';
import 'package:todolist/data/tasks_database.dart';
import 'package:uuid/uuid.dart';

class Task extends Equatable {
  final bool complete;
  final String id;
  final String note;
  final String title;

  Task(
    this.title, {
    this.complete = false,
    String? note = '',
    String? id,
  })  : this.note = note ?? '',
        this.id = id ?? Uuid().v4();

  Task copyWith({bool? complete, String? id, String? note, String? title}) {
    return Task(
      title ?? this.title,
      complete: complete ?? this.complete,
      id: id ?? this.id,
      note: note ?? this.note,
    );
  }

  @override
  List<Object> get props => [complete, id, note, title];

  @override
  String toString() {
    return 'Task { complete: $complete, title: $title, note: $note, id: $id }';
  }

  TaskItem toEntity() {
    return TaskItem(id: id, title: title, details: note, completed: complete);
  }

  static Task fromEntity(TaskItem item) {
    return Task(
      item.title,
      complete: item.completed,
      note: item.details,
      id: item.id,
    );
  }
}
